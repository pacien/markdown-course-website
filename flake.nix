# Build environment and recipe for the Markdown course website example.
#
# Author: Euxane TRAN-GIRARD
# Licence: CC BY-NC 4.0

{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    flaky-utils.url = "git+https://cgit.euxane.net/libs/flaky-utils";
  };

  outputs = { self, nixpkgs, flake-utils, flaky-utils }:
  flake-utils.lib.eachDefaultSystem (system:
  with nixpkgs.legacyPackages.${system};
  let
    texliveDist = texlive.combined.scheme-small;
    quartoDist = pkgs.quartoMinimal;
    solutionsSecret = "topsecret";

    compileQuarto = options: stdenv.mkDerivation {
      name = "quarto-book";
      src = ./.;
      nativeBuildInputs = [ quartoDist ];
      buildPhase = ''
        export HOME=$PWD
        ${options} quarto render
      '';
      installPhase = ''
        mkdir -p "$out"
        cp -r _book/* "$out/"
      '';
    };

    compilePandoc = src: to: options: stdenv.mkDerivation {
      name = "pandoc-output";
      inherit src;
      nativeBuildInputs = [ pandoc texliveDist ];
      installPhase = ''
        mkdir -p "$out"
        for f in *.md; do
          ${options} pandoc $f \
            --lua-filter ${./filters/strip-solutions.lua} \
            --output "$out"/$(basename $f .md).pdf \
            --to ${to}
        done
      '';
    };

  in {

    devShell = flaky-utils.lib.mkDevShell {
      inherit pkgs;
      tools = [ quartoDist pandoc texlive ];
    };

    packages = rec {
      pdf-slides = compilePandoc ./lectures "beamer" "";
      pdf-exercises = compilePandoc ./exercises "pdf" "STRIP_SOLUTIONS=1";
      pdf-solutions = compilePandoc ./exercises "pdf" "";

      website-public = compileQuarto "STRIP_SOLUTIONS=1";
      website-private = compileQuarto "";

      # Everything combined
      default = runCommand "combined" { } ''
        cp -Lr --no-preserve=mode ${website-public} "$out"
        cp -Lr --no-preserve=mode ${website-private} "$out/${solutionsSecret}"
        cp -Lr ${pdf-slides}/* "$out/lectures"
        cp -Lr ${pdf-exercises}/* "$out/exercises"
        cp -Lr ${pdf-slides}/* "$out/${solutionsSecret}/lectures"
        cp -Lr ${pdf-solutions}/* "$out/${solutionsSecret}/exercises"
      '';
    };

  });
}
