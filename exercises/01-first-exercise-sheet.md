---
title: "First exercises sheet"
documentclass: scrartcl
---

# Intro

This exercise sheet is compiled into:

* This HTML page on the course's website, for people having a tablet.
* A PDF for printing.

Versions with solutions are also generated but kept private for the teaching
assistants.


# Exercise 1

1. Prove either $P = NP$ or $P \neq NP$.

::: secret-solution
(This is only visible in the "secret" version of the website and PDFs).

$P = 0$ or $N = 1$.
Quite Easily Done. Or maybe not.
:::
