---
title: "First lecture"
---

# First chapter

## Intro

This document is compiled into:

* Beamer slides (PDF) for the actual lecture, and
* An HTML page on the course's website for easy access during exercise
  sessions.


# Second chapter

## Math

LaTeX math expressions can be used here:

$$ f(x)=\sum_{n=0}^\infty\frac{f^{(n)}(a)}{n!}(x-a)^n $$

## Code listing

An example of code listing with syntax highlighting:

```python
def hello():
    print('hello world')
```
