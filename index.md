---
title: "About"
---

# Markdown course website example / template

This is an example of course website written in Markdown.

Inputs:

* Markdown files for both the slides and exercises sheets, all pushed in a
  private git repository.

Outputs:

* A public website with flattened slides and exercises sheets without the
  solutions, for the students.

* A private website with the solutions to the exercises, for the teaching
  staff, at a "secret" address defined in `flake.nix`.

* PDF Beamer presentations for the lectures.
  Those can be downloaded from the website by replacing `.html` with `.pdf` in
  the URL.

* PDF exercise sheets, with and without solutions, for printing for work
  sessions without a computer.
  Those can be downloaded from the website by replacing `.html` with `.pdf` in
  the URL.


## Links

- Deployed site:
  - Public (for students):
    <https://euxane.gitpages.univ-eiffel.fr/markdown-course-website>
  - Private (with exercise solutions, for the teachers):
    <https://euxane.gitpages.univ-eiffel.fr/markdown-course-website/topsecret>

- Source repository:
  <https://gitlab.univ-eiffel.fr/euxane/markdown-course-website>


## Behind the scene

Pandoc is the engine used to process and compile the Markdown source files into
HTML and PDF through LaTeX. This engine allows easy transformation of the
Abstract Syntax Tree of the processed documents, which is used to strip the
solutions from the public exercises sheets for example.

Quarto is used to generate the website with page navigation and search. Its
more advanced features such as code execution aren't used here.

Nix is used to manage the build process and its dependencies. Everything is run
within a controlled sandbox, which can be run locally or remotely in a
reproducible manner.

The GitLab platform is used to host the git source repository. The website and
PDFs are automatically built for each push through GitLab CI. The website in
particular is hosted and published, and updated automatically using GitLab
Pages.


## References

- [Markdown syntax](https://daringfireball.net/projects/markdown/syntax)
- [Pandoc (manual)](https://pandoc.org/MANUAL.html)
- [Quarto (reference)](https://quarto.org/docs/reference/index.html)
- [Nix guides](https://nixos.org/learn.html)
- [GitLab CI configuration reference](https://docs.gitlab.com/ee/ci/yaml/)


## Licence

The code of this example is licensed under the [CC BY-NC 4.0].

It can be used as a template for course websites, without requiring the
publication of the added original content (which can may contain exercise
solutions not meant to be shared publicly).

[CC BY-NC 4.0]: https://creativecommons.org/licenses/by-nc/4.0/

